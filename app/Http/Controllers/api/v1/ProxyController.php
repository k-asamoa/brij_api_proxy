<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Models\ApiLog;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProxyController extends Controller
{
    public function receiveRequest(Request $request){
        $provider = $request->header('provider');
        $format = $request->header('format');
        $request_type = $request->header('provider_request_type');
        $provider_url = $request->header('provider_url');


        if ($format == 'xml'){
            $payload = $request->getContent();
        }else{
            $payload = $request->all();
        }

        $log_array = [
            'request_url' => $provider_url,
            'request_type' => $request_type,
            'request_body' => $payload,
            'source' => $provider
        ];

        try {

            $guzzle_client = new \GuzzleHttp\Client();
            if ($provider == 'MTN'){
                $response = $guzzle_client->post(
                    $provider_url, [
                        'body' => $payload,
                        'headers' => ['Content-Type' => 'text/xml'],
                        'connect_timeout' => 650,
                        'cert' => storage_path('MtnCerts/staging/certnew.cer'),
                        'ssl_key' => storage_path('MtnCerts/staging/brij_mtn.pem'),
                    ]
                );
            }else{
                $response = $guzzle_client->post(
                    $provider_url, [
                        'body' => json_encode($payload),
                        'headers' => ['Content-Type' => 'application/json'],
                        'connect_timeout' => 650,
                    ]
                );
            }
            $res = $response->getBody()->getContents();


            $log_array['response_body'] = $res;

            if ($provider == 'MTN'){
                Log::channel('mtn')->info(json_encode($log_array));
            }else{
                Log::channel('wema')->info(json_encode($log_array));
            }

        }catch (BadResponseException $exception) {
            $res = json_decode($exception->getResponse()->getBody()->getContents(), true);

            if ($provider == 'MTN'){
                Log::channel('mtn')->info(json_encode($res));
            }else{
                Log::channel('wema')->info(json_encode($res));
            }
        }


        return $res;

    }
}
