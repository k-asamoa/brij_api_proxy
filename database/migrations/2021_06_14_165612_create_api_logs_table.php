<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApiLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_logs', function (Blueprint $table) {
            $table->id();
            $table->string('request_url')->nullable();
            $table->string('request_type')->nullable();
            $table->text('request_body')->nullable();
            $table->text('response_body')->nullable();
            $table->string('source')->nullable();
            $table->string('callback_url')->nullable();
            $table->integer('callback_status')->default(0);
            $table->string('request_id')->nullable();
            $table->string('format')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_logs');
    }
}
